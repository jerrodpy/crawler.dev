<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PagesRepository")
 */
class Pages
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\Column(type="integer", name="number_of_tags")
     */
    private $NumberOfTags;

    /** Количество мили секунд
     * @ORM\Column(type="integer", name="processing_time")
     */
    private $ProcessingTime;

    /**
     * @ORM\Column(type="integer", name="depth")
     */
    private $depth;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getNumberOfTags(): ?int
    {
        return $this->NumberOfTags;
    }

    public function setNumberOfTags(int $NumberOfTags): self
    {
        $this->NumberOfTags = $NumberOfTags;

        return $this;
    }

    public function getProcessingTime(): ?int
    {
        return $this->ProcessingTime;
    }

    public function setProcessingTime(int $ProcessingTime): self
    {
        $this->ProcessingTime = $ProcessingTime;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDepth(): ?int
    {
        return $this->depth;
    }

    /**
     * @param mixed $depth
     */
    public function setDepth($depth): void
    {
        $this->depth = $depth;
    }


}
