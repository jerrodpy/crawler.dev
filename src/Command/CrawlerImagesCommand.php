<?php

namespace App\Command;

use App\Service\ParsContent;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CrawlerImagesCommand extends Command
{
    protected static $defaultName = 'app:crawler-images';

    private $parsContent;

    protected function configure()
    {
        $this
            ->setDescription('crawler изображение')
            ->addArgument('host', InputArgument::REQUIRED, 'Домен для парсинга')
            ->addOption('depth-parsing', null, InputOption::VALUE_OPTIONAL, 'Макимальная глубина парсинга')
            ->addOption('max-quantity-image', null, InputOption::VALUE_OPTIONAL, 'Максимальное количество изображений которое нужно спарсить');
    }


    public function __construct(ParsContent $parsContent)
    {
        $this->parsContent = $parsContent;

        parent::__construct();
    }


    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $host = $input->getArgument('host');

        if ($host) {
            $io->note(sprintf('Домен для парсинга: %s', $host));
        }

        $depthParsing = $input->getOption('depth-parsing');
        $maxQuantityImage = $input->getOption('max-quantity-image');

        if ($depthParsing) {
            $io->note(sprintf('Макимальная глубина парсинга: %s уровня', $depthParsing));
        }

        if ($maxQuantityImage) {
            $io->note(sprintf('Максимальное количество изображений которое нужно спарсить: %s изображений', $maxQuantityImage));
        }


        $result = $this->parsContent->crawlingPages($host, $depthParsing, $maxQuantityImage);


        $io->success(sprintf('Парсинг завершили (%s). Результаты можно увидить на странице /', $result));

        /*
         Открытые вопросы:
            - проверка урлов на предмет уже просканированого, т.к. при наличии дублей  сканирование может зациклится
            - получение контента file_get_content работает не всегда, зависит от сайта, cURL работает стабильнее,
              но опять таки разные сайты могут требовать разных параметров для успешного получения контента


         * */

    }
}
