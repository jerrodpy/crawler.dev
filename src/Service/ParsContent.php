<?php
/**
 * Created by PhpStorm.
 * User: jerrod
 * Date: 08.02.19
 * Time: 15:58
 */

namespace App\Service;

use App\Entity\Pages;
use Doctrine\ORM\EntityManager;
use SplPriorityQueue;
use Symfony\Component\DomCrawler\Crawler;

class ParsContent
{

    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param $url
     * @return bool|string
     * @throws \Exception
     */
    private function getContent($url)
    {
//        return file_get_contents($url);
        return $this->getCurlContent($url);
    }


    /** Получение контента средствами cURL
     * @param string $url
     * @return bool|string
     * @return string
     * @throws \Exception
     */

    private function getCurlContent(string $url): string
    {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Googlebot/2.1 (+http://www.google.com/bot.html)');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_EXPECT_100_TIMEOUT_MS, 15000);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 25);
        curl_setopt($ch, CURLOPT_COOKIEFILE, '/tmp/cookie.txt');
        curl_setopt($ch, CURLOPT_COOKIEJAR, '/tmp/cookie.txt');

        curl_setopt($ch, CURLOPT_VERBOSE, false);

        $res = curl_exec($ch);

        if (!$res) {
            throw new \Exception('Error Curl ' . curl_error($ch) . '(' . curl_errno($ch));
        }

        curl_close($ch);

        return $res;
    }

    /** Выполняем проверку наличие хоста в урле
     * @param string $host
     * @param string $url
     * @return bool
     */
    private function checkUrl(string $host, string $url)
    {

        if (0 === strpos($url, $host)) {
            return true;
        }

        return false;
    }


    /** Обходит в цикле страницы до указаной глубины или выборе указаного количества изображений
     * @param string $url
     * @param int|null $depth Максимальная глубина парсинга
     * @param int|null $quantity Максимальное количество изображений
     * @return int
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function crawlingPages(string $url, int $depth = null, int $quantity = null)
    {

        $currentDepth = 0; // предполагая что глубина сайта не более указаного числа
        $currentQuantity = 0;

        $queue = new SplPriorityQueue();
        $queue->setExtractFlags(SplPriorityQueue::EXTR_BOTH);

        $queue->insert($url, 0);

        while ($queue->valid()) {

            // $currentDepth изменяется от 0 в отрицательную сторону, т.е. абсолютное значение числа равно глубине разобраных страниц
            if ($depth && ($currentDepth + $depth) < 0) {

                return 'Обработали страницы до указаной глубины';
            }


            // прерываем перебор при выборке нужного количества изображений
            if ($quantity && $currentQuantity >= $quantity) {

                return 'Выбрали необходимое количество изображений';
            }

            $this->parsRow($queue, $currentDepth, $currentQuantity);
        }

        return 'END';

    }

    /** Парсим одину страницу  урл которой вынимаем из очереди, все найденые ссылки добавляем в ту же очередь
     * но с другим приоритетом а текущий урл с количеством картинок сохраняем в БД
     * @param SplPriorityQueue $queue
     * @param int $currentDepth
     * @param int $currentQuantity
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function parsRow(SplPriorityQueue &$queue, int &$currentDepth, int &$currentQuantity)
    {

        $row = $queue->extract();

        $currentDepth = $row['priority'];
        $url = $row['data'];
        $newPriority = $currentDepth - 1;

        $startTime = microtime(true);

        $html = $this->getContent($url);

        $crawler = new Crawler($html);
        $elemLink = $crawler->filter('a');
        $elemImage = $crawler->filter('img');

        $numberImage = $elemImage->count();

        $currentQuantity += $numberImage;

        foreach ($elemLink as $domElement) {

            $nextUrl = $domElement->getAttribute('href');

            if ($this->checkUrl($url, $nextUrl)) {

                $queue->insert($domElement->getAttribute('href'), $newPriority);
            }
        }


        $deltaTime = $this->deltaTime($startTime, microtime(true));

        $this->savePageData($url, $numberImage, $deltaTime, abs($currentDepth));
    }

    /** Вычисляет разность времени и переводит в миллисекунды
     * @param float $startTime
     * @param float $endTime
     * @return int Целое число миллисекунд
     */
    private function deltaTime(float $startTime, float $endTime): int
    {
        return round(($endTime - $startTime) * 1000, 0);
    }

    /**
     * @param $url
     * @param $number
     * @param $time
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function savePageData($url, $number, $time, $depth)
    {

        $page = new Pages();

        $page->setUrl($url);
        $page->setNumberOfTags($number);
        $page->setProcessingTime($time);
        $page->setDepth($depth);

        $this->em->persist($page);
        $this->em->flush();
    }


}