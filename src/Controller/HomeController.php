<?php

namespace App\Controller;

use App\Entity\Pages;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {

        $pages = $this->getDoctrine()->getManager()->getRepository(Pages::class)->findAll();

        return $this->render('home.html.twig', array(
            'pages' => $pages,
        ));
    }
}
